<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content">
    <?php print $content ?>
  </div>

  <div class="blogfoot">
    
    <?php if ($taxonomy): ?>
      <span class="foot_top">Filed under: <?php print $terms ?></span><br/>
    <?php endif;?>

    <?php if ($links): ?>
      <span class="foot_bottom"><?php print $links; ?></span>
    <?php endif; ?>
    
  </div>

</div>
