<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
<title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>

<body>

<div id="container">
	<div id="rightside">
		
		<div id="title"><p><?php print $site_name; ?></p></div>
		
		<div id="sidebar">
		  
		  <ul class="nav">
			<?php if (isset($primary_links)) : ?>
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
            <?php endif; ?>
		  </ul>
		
          <?php if ($left): ?>
            <div id="sidebar-left" class="sidebar">
            <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
            <?php print $left ?>
            </div>
          <?php endif; ?>
          
          <?php if ($right): ?>
            <div id="sidebar-right" class="sidebar">
            <?php if (!$left && $search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
            <?php print $right ?>
            </div>
          <?php endif; ?>
        
        </div><!-- /sidebar -->
	
	</div><!-- /rightside -->
	
	
	<div id="main">
	
		<?php if ($header): ?>
            <div id="header"><?php print $header ?></div>
    	<?php endif; ?> 
        
		<div id="content">
    
    	  <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
		  <?php print $content ?>
          <?php print $feed_icons ?>
    
    	</div>
		
		<div id="footer">
		<?php if ($footer): ?>
            <?php print $footer ?>
            <div id="credits">&copy; <?php print $site_name; ?>
            <?php if ($footer_message): print ' : '. $footer_message ; endif; ?>
            </div>
        <?php endif; ?>
	
	</div> <!-- /main -->
	
	<div id="end">
	</div><!-- /end -->
	
	</div>
</div>

<?php print $closure ?>

</body>
</html>