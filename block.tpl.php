

<?php if (!empty($block->subject)): ?>
  <h2><?php print $block->subject ?></h2>
<?php endif;?>

  <div class="block_inside">
    <div class="content"><?php print $block->content ?></div>
  </div>
  
